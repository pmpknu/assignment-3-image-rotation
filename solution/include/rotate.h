/* FILE NAME : rotate.h
 * PROGRAMMER: pmpknu
 * DATE      : 18.11.2023
 * PURPOSE   : Rotate functions declaration module.
 */

#ifndef ROTATE_H
#define ROTATE_H

#include "./internal/image.h"

/* Creating a rotated to 90 degrees left copy of the image function.
 * ARGUMENTS:
 *   - source image:
 *       struct image const source;
 * RETURNS:
 *   (image) rotated image.
 */
struct image rotate(struct image const source);

#endif // !ROTATE_H
