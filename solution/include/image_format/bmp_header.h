/* FILE NAME : bmp_header.h
 * PROGRAMMER: pmpknu
 * DATE      : 19.11.2023
 * PURPOSE   : BMP header declaration module.
 */

#ifndef BMP_HEADER
#define BMP_HEADER


#define BMP_SIGNATURE 0x4d42    /* Signature of bmp file header */
#define BMP_HEADER_SIZE 40      /* Header size in bytes */
#define BMP_NO_COMPRESSION 0    /* No compression for biCompression */
#define BMP_DEFAULT_PLANES 1    /* Default number of color planes */
#define BMP_PELS_PER_METER 2835 /* 72 DPI */
#include  <stdint.h>

/* BMP header structure */
struct bmp_header
{
        uint16_t bfType;            /* signature of file */
        uint32_t bfileSize;         /* file size in bytes */
        uint32_t bfReserved;        /* should contain 0 */
        uint32_t bOffBits;          /* bits offset from header start */
        uint32_t biSize;            /* header size in bytes */
        uint32_t biWidth;           /* image width in pixels */
        uint32_t biHeight;          /* image height in pixels */
        uint16_t biPlanes;          /* the number of color planes (usually 1) */
        uint16_t biBitCount;        /* bites per pixel */
        uint32_t biCompression;     /* compression method */
        uint32_t biSizeImage;       /* size of bitmap image */
        uint32_t biXPelsPerMeter;   /* pixels per meter on x axis */
        uint32_t biYPelsPerMeter;   /* pixels per meter on y axis */
        uint32_t biClrUsed;         /* number of colors in the color palette */
        uint32_t biClrImportant;    /* number of important colors in the color palette (0 -- all are important) */
} __attribute__((packed));

#endif // !BMP_HEADER
