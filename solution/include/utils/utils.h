/* FILE NAME : utils.h
 * PROGRAMMER: pmpknu
 * DATE      : 20.11.2023
 * PURPOSE   : Utilities for work with images declaration module.
 */

#ifndef UTILS_H
#define UTILS_H

#include "../internal/image.h"
#include <stdint.h>

uint8_t calculate_padding(uint64_t width, uint64_t size);
void img_destructor(struct image* img);
void img_initializer(struct image* img);
#endif //!UTILS_H
