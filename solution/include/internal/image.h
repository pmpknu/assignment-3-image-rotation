/* FILE NAME : image.h
 * PROGRAMMER: pmpknu
 * DATE      : 18.11.2023
 * PURPOSE   : Internal image representation declaration module.
 */

#ifndef IMAGE_H
#define IMAGE_H
#include <stdint.h>

#include "pixel.h"

/* Pure image structure */
struct image {
  uint64_t width, height; // width and height of image
  struct pixel* data;     // an array of pixels representing the image itself
};

#endif // !IMAGE_H
