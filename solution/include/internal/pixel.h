/* FILE NAME : pixel.h
 * PROGRAMMER: pmpknu
 * DATE      : 29.12.2023
 * PURPOSE   : Internal pixel representation declaration module.
 */

#ifndef PIXEL_H
#define PIXEL_H
#include <stdint.h>

/* Pixel structure */
struct pixel {
  uint8_t b, g, r; // blue, green and red pixel's components
} __attribute__((packed));

#endif // !PIXEL_H
