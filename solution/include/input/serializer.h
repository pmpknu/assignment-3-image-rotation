/* FILE NAME : serializer.h
 * PROGRAMMER: pmpknu
 * DATE      : 19.11.2023
 * PURPOSE   : Input format serializer declaration module.
 */

#ifndef SERIALIZER_H
#define SERIALIZER_H

#include "internal/image.h"
#include <stdio.h>

/* serializer */
enum  write_status  {
  WRITE_OK = 0,
  WRITE_HEADER_ERROR,
  WRITE_BITS_ERROR
  /* other errors codes */
};

/* Writing a BMP image to file function.
 * ARGUMENTS:
 *   - file pointer:
 *       FILE* out;
 *   - image containing write data:
 *       struct image const* img;
 * RETURNS:
 *   (write_status) action status
 */
enum write_status to_bmp( FILE* out, struct image const* img );

#endif // !SERIALIZER_H
