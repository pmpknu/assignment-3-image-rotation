/* FILE NAME : deserializer.h
 * PROGRAMMER: pmpknu
 * DATE      : 19.11.2023
 * PURPOSE   : Input format deserializer declaration module.
 */

#ifndef DESERIALIZER_H
#define DESERIALIZER_H

#include "internal/image.h"
#include <stdio.h>

/*  deserializer   */
enum read_status
{
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_CANT_ALLOCATE,
  READ_INVALID_OFFSET
  /* other error codes */
};

/* Reading a BMP image data function.
 * ARGUMENTS:
 *   - read file pointer:
 *       FILE* in;
 *   - image containing read data:
 *       struct image* img;
 * RETURNS:
 *   (read_status) action status.
 */
enum read_status from_bmp( FILE* in, struct image* img );

#endif // !DESERIALIZER_H
