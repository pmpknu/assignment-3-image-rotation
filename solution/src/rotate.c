/* FILE NAME : rotate.c
 * PROGRAMMER: pmpknu
 * DATE      : 26.11.2023
 * PURPOSE   : Rotate functions implementation module.
 */

#include "../include/internal/image.h"
#include "../include/internal/pixel.h"
#include <stdlib.h>

struct image rotate(struct image const source)
{
    struct image rotated;

    rotated.height = source.width;
    rotated.width = source.height;
    rotated.data = malloc( sizeof( struct pixel ) * source.height * source.width);

    if (rotated.data == NULL)
        return rotated;

    for (size_t i = 0; i < source.height; i++)
        for (size_t j = 0; j < source.width; j++)
            rotated.data[source.height * (source.width - j - 1) + i]
                = source.data[source.width * i + j];

    return rotated;
}

