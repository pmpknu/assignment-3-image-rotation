/* FILE NAME : utils.h
 * PROGRAMMER: pmpknu
 * DATE      : 20.11.2023
 * PURPOSE   : Utilities for work with images implementation module.
 */

#include "../../include/utils/utils.h"
#include "../../include/internal/image.h"
#include <stdlib.h>

#define BMP_IMG_ALIGN 4

uint8_t calculate_padding(uint64_t width, uint64_t size)
{
    return ( (BMP_IMG_ALIGN - ( width * size ) % BMP_IMG_ALIGN) % BMP_IMG_ALIGN);
}

void img_initializer(struct image* img) {
    img->data = NULL;
    img->height = 0;
    img->width = 0;
}

void img_destructor(struct image* img)
{
    if (img != NULL) {
        if (img->data != NULL) {
            free(img->data);
            img->data = NULL;
        }
    }
}
