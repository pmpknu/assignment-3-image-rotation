/* FILE NAME : serializer.h
 * PROGRAMMER: pmpknu
 * DATE      : 20.11.2023
 * PURPOSE   : Input format serializer implementation module.
 */


#include <stdio.h>

#include "./../../include/internal/pixel.h"
#include "./../../include/image_format/bmp_header.h"
#include "./../../include/input/serializer.h"
#include "./../../include/utils/utils.h"

enum write_status to_bmp( FILE* out, struct image const* img )
{
    uint8_t padding = calculate_padding(img->width, sizeof( struct pixel ));
    uint64_t row_size = img->width * sizeof( struct pixel ) + padding;

    struct bmp_header header = {
            .bfType = BMP_SIGNATURE,
            .bfileSize = (uint32_t)( sizeof(struct bmp_header) + img->height * row_size ),
            .biSizeImage = (uint32_t)( img->height * row_size ),
            .bfReserved = 0,
            .bOffBits = sizeof( struct bmp_header ),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = (uint32_t)img->width,
            .biHeight = (uint32_t)img->height,
            .biPlanes = BMP_DEFAULT_PLANES,
            .biBitCount = sizeof( struct pixel ) * 8,
            .biCompression = BMP_NO_COMPRESSION,
            .biXPelsPerMeter = BMP_PELS_PER_METER,
            .biYPelsPerMeter = BMP_PELS_PER_METER,
            .biClrUsed = 0,
            .biClrImportant = 0
    };

    if (!fwrite(&header, sizeof(header), 1, out)) {
        return WRITE_HEADER_ERROR;
    }

    /* writing pixels */
    for (size_t i = 0; i < img->height; i++) {
        if (!fwrite(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, out)) {
            return WRITE_BITS_ERROR;
        }
        fseek(out, padding, SEEK_CUR);
    }

    return WRITE_OK;
}
