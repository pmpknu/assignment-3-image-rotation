/* FILE NAME : deserializer.h
 * PROGRAMMER: pmpknu
 * DATE      : 19.11.2023
 * PURPOSE   : Input format deserializer implementation module.
 */


#include "./../../include/internal/pixel.h"
#include "./../../include/image_format/bmp_header.h"
#include "./../../include/input/deserializer.h"
#include "./../../include/utils/utils.h"
#include <stdio.h>
#include <stdlib.h>



enum read_status from_bmp( FILE* in, struct image* img )
{
  /* Image header containing information */
  struct bmp_header header;

  /* Read header from file */
  size_t header_items_read = fread(&header, sizeof(struct bmp_header), 1, in);
  if (header_items_read == 0)
    return READ_INVALID_HEADER;

  /* Checking read signature */
  if (header.bfType != BMP_SIGNATURE)
    return READ_INVALID_SIGNATURE;

  /* Checking that header read correctly */
  if (header.bOffBits < sizeof( header ))
    return READ_INVALID_OFFSET;

  /* Set image properties and allocate data */
  img->height = header.biHeight;
  img->width = header.biWidth;
  size_t img_data_size = sizeof(struct pixel) * img->height * img->width;
  img->data = malloc(img_data_size);
  if (img->data == NULL)
    return READ_CANT_ALLOCATE;

  /* Calculate file padding */
  uint8_t padding = (uint8_t)calculate_padding(img->width, sizeof( struct pixel ));

  /* Skip data to get raster array */
  fseek(in, (long)header.bOffBits, SEEK_SET);

  /* Read pixels from file */
  for (size_t i = 0; i < img->height; i++)
  {
    const size_t read_items =
            fread(img->data + i * img->width, img->width * sizeof(struct pixel), 1, in);

    if (read_items == 0)
    {
      /* Clear allocated memory */
      img_destructor(img);
      return READ_INVALID_BITS;
    }
    fseek(in, padding, SEEK_CUR);
  }
  return READ_OK;
}
