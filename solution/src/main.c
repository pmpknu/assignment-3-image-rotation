#include <stdio.h>
#include <stdlib.h>

#include "./../include/input/serializer.h"
#include "./../include/input/deserializer.h"
#include "./../include/rotate.h"
#include "./../include/utils/utils.h"

#define PI 180

#define EXIT_CLEAR(in, out, src, dst) \
    if ((in) != NULL) fclose((in));   \
    if ((out) != NULL) fclose((out)); \
    img_destructor(&(dst))

enum error_codes {
    ERROR_WRONG_ARGS,
    ERROR_BAD_ANGLE,
    ERROR_CANT_READ,
    ERROR_CANT_WRITE,
    ERROR_CANT_ALLOCATE
};

int main( int argc, char** argv ) {
    FILE *in = NULL;
    FILE *out = NULL;
    struct image src, dst;
    enum read_status read_st;
    enum write_status write_st;

    int32_t angle = atoi(argv[3]);

    if (argc != 4)
    {
        fprintf(stderr, "4 arguments expected.\n");
        EXIT_CLEAR(in, out, src, dst);
        exit(ERROR_WRONG_ARGS);
    }

    if (abs(angle) > (3 * PI / 2) || angle % (PI / 2) != 0)
    {
        fprintf(stderr, "Bad angle value.\n");
        EXIT_CLEAR(in, out, src, dst);
        exit(ERROR_BAD_ANGLE);
    }

    if (angle < 0)
        angle += 2 * PI;

    if ((in = fopen(argv[1], "rb")) == NULL) {
        fprintf(stderr, "Cannot open file: %s\n", argv[1]);
        EXIT_CLEAR(in, out, src, dst);
        exit(ERROR_CANT_READ);
    }

    if ((out = fopen(argv[2], "wb")) == NULL) {
        fprintf(stderr, "Cannot open file: %s\n", argv[1]);
        EXIT_CLEAR(in, out, src, dst);
        exit(ERROR_CANT_WRITE);
    }

    img_initializer(&src);
    img_initializer(&dst);

    if (( read_st = from_bmp(in, &src) ) != READ_OK)
    {
        fprintf(stderr, "Error while reading from file: %d", read_st);
        EXIT_CLEAR(in, out, src, dst);
        exit(ERROR_CANT_READ);
    }

    dst = src;
    for (size_t i = 0; i < angle / 90; i++) {
        struct pixel* old = dst.data;

        dst = rotate(dst);
        if (dst.data == NULL) {
            fprintf(stderr, "Error can't allocate while rotate image");
            EXIT_CLEAR(in, out, src, dst);
            exit(ERROR_CANT_ALLOCATE);
        }
        if (old != NULL) free(old), old = NULL;
    }


    if (( write_st = to_bmp(out, &dst) ) != WRITE_OK)
    {
        fprintf(stderr, "Error while writing to file: %d", write_st);
        EXIT_CLEAR(in, out, src, dst);
        exit(ERROR_CANT_WRITE);
    }

    EXIT_CLEAR(in, out, src, dst);

    return 0;
}
